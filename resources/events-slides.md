# How to create an event the free and libre open source way?

<small>We'll do this in English and rather outside of doku.ccc.de, as we'd like to include groups and event organizers from all around the world. </small>

---

'''Underlaying is the assumption''':
* IF the group getting together is coming from the "open source culture" 
* THEN it doesn't matter what "surrounding culture" they belong to: 
* => Then they are able to create an event from those patterns (more or less ad-hoc, depending on the size). 

---

# Group

* alice, smtw, sva, ilu
* Reach: IRC #EventHowTo
* Tools: HackMD, gitlab

---

# New Members

Invite! Everyone can join :)

---


The **goal is to create a guide / howto / list of patterns** which support every group that wants to create a community-/volunteer-driven event (where everyone contributes a bit to make it happen; like it happens in FLOSS). 

(Ideal: Scaling from small number of participants up to thousands)

---

**A dream is a *wizard***: clicking through it; defining your requirements and needs: 

As a result you get patterns you could consider following, maybe even somewhat of a "checklist" of things you have to take care of. (Wizard like e.g. Wikimatrix)

---

Breaking down the unit "event" to its main base: 
* People (who?)
* Place (where?)
* Content (why?)
* Time (when?) (fix)

---

Which results in the following first-level-questions: 
* why (eg reason, something happened) 
* who (team, sender/receiver, invitations) 
* where (indoor/outdoor, climate, city/countryside)
* what (type & form)
* when (season, availability

---

If your group found answer or fairly clear directions on those, figure out details: 

* how many 
* how big
* how long
* what goal
* what /how invitation/communication?
* how organized / what entity, if any at all?
* logistics (get stuff to/away the event)

---

No stopper found in that list? Now you could already announce that your event happens :)

Now browse through requirements: 


---

* infrastructure
    * - [ ] net
    * - [ ] secu
    * - [ ] recordings
    * - [ ] accommodation for people and/or volunteers
    * - [ ] dect / gsm
    * - [ ] projector
    * - [ ] PA system
    * - [ ] Lights
    * - [ ] Power supply and sockets enough?
        * [ ]provided by venue?
        * [ ]special treatment needed (like on a campsite)?
    * - [ ] Toilets
        * [ ]provided by venue?
        * [ ]special treatment needed (like on a campsite)?
    * - [ ] waste management
        * [ ] provided
        * [ ] special treatment neede
    * - [ ] water / wastewater
        * [ ] provided
        * [ ] drinkable water
        * [ ] freshwater
        * [ ] graywater
* personal
    * - [ ] food for people and/or volunteers 
    * - [ ] volunteer => definition of event we design.... yes
    * - [ ] awarenessteam / coc / etc
    * - [ ] tickets / presale
        * [ ] payment processeing
    * [ ] participant management
        * [ ] just visitors
        * [ ] participants do $stuff (like assemblies)
* - [ ] website
    * - [ ] contact / feedback
    * - [ ] 
* - [ ] signup / reservations
* - [ ] outreach
* - [ ] location/venue

---

# Result

List of steps/things/patterns you'd consider following, 

to reach the ideas you defined with answering the questions before.

---

#### Note on Money handling

Base pattern of creating an event the open source way doesn't include any handling of money at all. Otherwise: Money in, money out, money handling. 

We'll collect various patterns of money handling, like https://www.ctrlc.hu/~stef/rfc-cpp.txt or https://attic.hillhacks.in/contribution

---

#### Note on calendar

This is a sub-group within this, fixing the problem that "our community" doesn't have any useful calendar at all, besides some regional focused ones. Find more on https://md.gnom.space/radar


---

### next

* <del>creating sheet of event-tools / software and services</del>
* creating one point of start (not-dokuwiki)
* structure and change processes (Figuring where to host this)
* filling list of 2nd level questions
* filling list of requirements
* collecting useful links / other howtos and similars
* collection and first-contact of relevant/interesting groups this can be shared to
* collection of noteworthy events worldwide

---

### Then
* check the list with events we have been involved to

### After
* Share this to familiar groups, re-check and iterate
* Widen up the group 


