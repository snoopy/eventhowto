# Event Patterns


How to create an event the opensource way.
Collecting existing material for common problems and solutions. 

# 0 event decision

## 1. 1st level questions

* why
    * reason-pattern: something happened :P (eg new technology, law, movement)
    * date-pattern: there is eg an annivarsary on something 
    * change-pattern: something happened that needs action/change
    * motivation for the event
* who
    * [team](#team) (decisions, structure, responsibility [take and give], trust)
    * [invitation](#invitation) open, public, invite only, members only, special-skills only, sold-out
    * [participant](#participant) sender and receiver, helper (all participants)
* what
    * type/style: education, knowledge sharing, (un-)conference, barcamp, partcipatory lecture, show & visitor-pattern, community-driven
* where
    * [location](#location) indoor/outdoor, climate, city/countryside, inner city / outer city
* when
    * season, happening/reason, calender-events (eg public holidays), availability (people)

## 2. 2nd level questions
* how many 
* how big
* how long
* what goal
* where exactly (possible locations available fitting the requirements?)
* what /how invitation
* how organized / what entity, if any at all?
* how to keep contact between organizers and rest? (volunteers + visitors + participants, etc)
* needed infrastructure
* logistics (get stuff to/away the event location)
* (who is taking which responsibility? Is there at least one possibility per task?)
* do you want a design and / or motto / slogan, consider selling merchandise
* think about options to finance the event
* collect regulations for events in your country / area / venue
    * some states have huge burocracies, get used to that for a successful event

if no stoppers found here: start planning your event :)

# 1 event planning

## Requirements (leading to tasks)

### 1st level
* why: write a mission statement
* who: 
    * Team: fix at least a few people being the committed core-team for the whole duration of the event
    * invitation: Whom to invite to come?
    * participant: Whom to invite to speak and participate?
* what: decide on a particular structure for your event and its content
* where: start visiting posssible location that are free for your possible dates
* when: fix a date (or a few possible ones)

### second level

* why: publish your mission statement e.g. on a website, ask for feedback
    * maybe involve press
* who: have your team coming together on ML, chats, RL-meets, phone-conferences
* what: develop the structure of your event and the expected content
* where: decide on a venue and book it - invest time to get in touch with venue staff, it will help a lot when it get stressier
* when: decide on a date

=> publish an invitation for your event (includes CFP ideally) on web, posters, flyers, MLs that fits the topic, Universities, groups around the topic, whatever. 

now start going into more detail:


### yes/no questions

*note* https://doku.ccc.de/Veranstaltung/Organisation

Each task in this list needs to be done by a person/team, also you need to create a timeline of what needs to be done when (and ideally by whom). For timeline also consider deadlines by law eg.
NOTE: 
* who is taking which responsibility? Is there at least one possibility per task?
* what tools/software are suitable for the choosen tasks, teams, responsibilities?

* - [ ] website
    * - [ ] where & when 
    * - [ ] mission statement / general invitation / about
    * - [ ] contact
    * - [ ] how to participate
        * - [ ] as volunteer (call for volunteers)
        * - [ ] as speaker (call for papers)
        * - [ ] as workshop-host (call for hands-on)
        * - [ ] as co-organizer
        * - [ ] as sponsor/funder
        * - [ ] as exhibitor
        * - [ ] 
    * - [ ] ticket / signup 
    * - [ ] FAQ / what is allowed / wanted / any kind of relevant additional information eg on venue 
    * - [ ] travel & accomodation infos
    * - [ ] schedule, calendar
    * - [ ] feedback

* infrastructure online
    * - [ ] website / hosting
    * - [ ] signup / payment processing 
    * - [ ] Mailing list or similiar asynchronous medium
    * - [ ] Chat / Messanger or similar synchronous medium
    * - [ ] wiki / pads / hackMD or similiar (collaborative) documentation medium
        * - [ ] list of important phone number and contacts
    * - [ ] tracker (for requests and tasks) or similiar tracking medium

* material / infrastructure physical
    * - [ ] accommodation for volunteers and / or attandees
        * [ ] provided by venue?
        * [ ] check for cheap group accomodations (sporthalls, schools, eg)
        * [ ] tents or similar if outdoors
    * - [ ] network infrastructure (including uplink)
    * - [ ] recordings of sessions/talks
    * - [ ] radio / dect / gsm
    * - [ ] moneyhandling / cashdesk / entrance (may incl. merch)
    * - [ ] infodesk where attendees can ask anything
    * - [ ] Power supply (including "uplink") and sockets
        * [ ] provided by venue?
        * [ ] special treatment needed (like on a campsite)?
    * - [ ] Toilets
        * [ ] provided by venue?
        * [ ] special treatment needed (like on a campsite)?
    * - [ ] waste management
        * [ ] provided by venue?
        * [ ] special treatment needed
    * - [ ] water / wastewater
        * [ ] provided by venue?
        * [ ] freshwater / drinkable water
        * [ ] grey- an wastewater
    * - [ ] stage / workshop equipmient
    	* - [ ] projector
    	* - [ ] PA system
    	* - [ ] Lights
    	* - [ ] whiteboards, flip charts, moderatation tools (pen, paper, eg)
    * - [ ] furniture (tables, chairs, speakerdesks, ...)
        * [ ] provided by venue?
        * [ ] checking on rental options
    * - [ ] kitchen - drinks and food for volunteers and / or attandees
        * [ ] provided by venue?
        * [ ] own kitchen crew
        * [ ] caterer
    * [ ] decorations / installation / art
    * [ ] tools / small workshop (things will break)
    * [ ] onsite (computer) systems (for workshops, orga teams, eg)
    * [ ] printer(s) for small and maybe big things to print out
    * [ ] scizzers, pen and paper 

* planning teams and materials for implementation
    * - [ ] security (also fire regulations, medical help eg)
    * - [ ] food for people and/or volunteers 
    * - [ ] drinks for people and/or volunteers 
    * - [ ] volunteer management
    * - [ ] content creation (jury for deciding on submissions and creating the schedule)
    * - [ ] awarenessteam / coc / etc
    * - [ ] venue structure (what rooms/spaces for what purpose) and furniture 
    * - [ ] finance team

# 2 event implementation
* - [ ] timetable when needs what to happen for build up, think about what tasks are depending from each other (like, setting up network needs working power supply - working people need food and accommodation)
    * - [ ] who is responsible for the particular task
* - [ ] document the status of the space "as you get"
* - [ ] Logistics (getting stuff there), unpacking and sorting it, also define particular storage spaces 
* - [ ] build up things, fix broken things :)
* - [ ] signs and maps (where to find what)
* - [ ] be okay if not everything works out as expected - get together once in a while to redecide on plans. Keep in mind that you have a hard deadline. 
* - [ ] work thight with venue staff


# 3 event happening
* - [ ] Timetable for shifts of teams, responsibilities and volunteers, keep them up-and-running. Have in mind that some teams will have peak-times (eg entrance, wardrobe, kitchen)
* - [ ] take care of attendees and what happens on your event
* - [ ] be prepared for unexpected happenings and quick-fixing
* - [ ] have at least one orga team member on site while event is running
* - [ ] consider short daily orga meetup
* - [ ] blog post / mail / press release
* - [ ] take care of venue and their staff

# 4 after the event
* - [ ] tear down timetable (who, what and where)
* - [ ] logistics, getting things away
* - [ ] cleanup incl. compare state with "as you get" stat (see #2)
* - [ ] blog post / mail / press release
* - [ ] ask attandees for feedback
* - [ ] sort finances
* - [ ] psot mortem analysis (what was good, what okay, what bad. docuemnt that) - also with venue





## Team

### Orga
Like the [hackerspaces Design Pattern](https://wiki.hackerspaces.org/The_Critical_Mass_Pattern) suggested, a Team of 2+2 is the minimum for an effecitve event organisation. It's higly recommended to define fix contact Persons. Depending on the Event size natrually the Teamsize will grow and will start splitting into serveral (Sub)Teams. Coordination between Teammembers and Teams is essential. Choose a Communication channel and stick to it until Event end.
FIXME: Informations about collaboration, team, communication
prepare for deadlines! you'll need to order stuff (food batches) and so on, printmaterial


### Staff
while you're event is growing, also the need of helping hands who "build" the event is growing. Nearly everything can be covered by Volunteers. A rule of thumb, for smaller events: 10 angel to 100 participants. In addition, two coordinators, who already have some experience. Pen, Paper & Whiteboard works well for up to 300 participants, [Engelsystem](https://engelsystem.de/) can be used for larger events.

For some Infrastructure Tasks (especially on bigger events or spcial locations like campsites) you might consider to book commercial stuff. For example set up power supply in exhebition halls often subjected to regulations which can be pretty tought.


## location

(story that owners of venue will get more and more nervous, but will be fine latest after the event - usually!)


### infrastructure

#### Power
Make sure that the venue serves for all your power needs. This is especially important if ppl are invited to bring their (power-) tools, eg CNC-mills and 3D-Printers. It is adviceable to ask those groups beforehand what kind of power-consumers they’ll be bringing along. You will always need more power than you planned.

#### toilets / showers
Make sure there are enough toilets and (on multi-day events) washing possibilities.

#### Water
Have free drinking water (if the tap-water doesnt serve) 

#### Food
If you can, sell Club-Mate
can be a organisation prob depends of size, law and money (also for maybe caterer)

## Participant
Think about your audiance. Educate children for a day is different to a full week Conference. Take notes and set up goals for your event - even if goals means Party ;)

### Invitation
Ask yourself whom to invite: nobody for a complete open event, only the speakers or even every single attendee. You propably need managment tools for invitations, see FIXME LINK
Similar is ticketing. Even for a total free event you might need a "ticketing" system if your space is limited. 
Under aged and minor people (not small kids, legal foo)

### Signup
Decide if you need payments upfront and what you need to ask from attendees. 
Communicate shop opening times for the future, plan strategies for high wanted events. like 3 batches in different time slots. block a bunch for speakers, artists and verpeiler.

### Callout
Ask at your local hackerspace for support, create a Call For Participation and start to create a design. 


## Software
  * Software Matrix nach EventTasks https://md.gnom.space/events-pattern-softwaretable


