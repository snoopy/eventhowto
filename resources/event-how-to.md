# Event-How-To

* Creating an event the Free and Open Source way!
* Everyone contributes a smaller or bigger bit, so that as a result you get all the things done without anyone being involved full time. 

## Ongoing

* Questions: https://md.gnom.space/events-pattern
* information gathering and comparison of relevant tools and services [Software Matrix](https://md.gnom.space/events-pattern-softwaretable)

* public repo and web page on https://chaos.expert/EventHowTo
* This project in slide-format: https://md.gnom.space/p/events-slides#/
 



# preface

xxx This Text has been created on the Regio weekend in 2017 and is based on the event/organization, as well as on personal experiences from the Organization of past ETS.


* legal entity
* volunteer system (long term and short term volunteers)
* one
* everyone should be having fun with this


# Resources

## Tools

* (eg also volunteers)
* etherpad and hackmd
* mumble for phone conferences
* ethercalc
* irc / maillinglists
* wiki
* volunteers: https://engelsystem.de/ -- https://github.com/engelsystem/engelsystem

https://docs.google.com/spreadsheets/d/1KiQGiLHdRJWLGkSStZHRYcJRMfuuqCp-LzSq1qrR3qQ/edit#gid=0

* Calendar 
  * https://pads.ccc.de/events
  * https://md.gnom.space/radar


## Infrastructure
* https://eventinfra.org/ "EventInfra supports non-profit/community-run events by providing network infrastructures."

## Links
 * CCC
     * https://doku.ccc.de/Easterhegg/Organisation (german, behind htacces)
     * https://doku.ccc.de/Chaos_Communication_Congress/Ort#Checkliste_.2F_Anforderungen 
 * Camp++ 
     * community payment protocol: https://www.ctrlc.hu/~stef/rfc-cpp.txt
     * https://github.com/hsbp/ticketing (zero knowledge signup)
     * https://gist.github.com/stef/6496890
 * Hillhacks/hackbeach ressources
     * https://pads.hillhacks.in/Hillhacks2016-teams
     * https://pads.hackbeach.in/teams-hb-1
     * https://ethercalc.hillhacks.in/venue
 * Entropia / GPN:
     * https://entropia.de/GPN:Howto
* ticket verkauf systeme: 

* signup / registrierung
  * https://www.dokuwiki.org/plugin:bureaucracy (formulare,datenmanagement)

https://github.com/emfcamp/event-docs/blob/master/event-management-plan/event-management-plan.pdf

# Areas of organization / tasks 

xxx In General, it is good practice for an event of various Orgabereiche.
For work groups to these areas, it is recommended to define a fixed contact person.
Also, the inventory list for things that can be borrowed adhered to for all areas - this can save you considerably expenditure in purchase/rental fees. 


### Volunteers

xxxxx 
The event stands and falls with the angels. The angels are treated well! A separate area to Relax in would be good (for smaller Events such as the easterhegg necessarily relevant or possible lack of space?), the angels should also get a bun-flat-rate.

Be sure to have enough angels. A rule of thumb, for smaller events: 10 angel to 100 participants. In addition, two coordinators, who already have some experience. engelsystem.de can be used for larger events up to 300 participants with pen&paper to the wall or a Whiteboard.

xxxxx #### Accommodation for angels
Consider when and how many angels are minimally used. Please do not night-shifts and forget!



### Power

Make sure that the venue serves for all your power needs. This is especially important if ppl are invited to bring their (power-) tools, eg CNC-mills and 3D-Printers. It is adviceable to ask those groups beforehand what kind of power-consumers they'll be bringing along. 

Lots of powersockets are important. Tell the participants to bring them along, but you should also have a backbone of wiring. Maybe approach LAN-Party-People / Gamer-events if they have stuff.

You will always need more power than you planned. 


### Network / NOC

xxxxx
There are certain leihbare CCCV Hardware such as Switches and wireless APs, etc., The NOC has created chaos far and not only has the Hardware GmbH. The storage is organized in a decentralized manner. In The Case Of Bedarf:noc@ccc.de

abuse number you might wanna have for in- and outsiders to call.

Check the legal situation providing network for bigger groups in general for you country/place. 

what about the legal environment, eg if the participants are doing filesharing or any proper "hacking"?

eg for camping its not needed to have LAN everywhere, WLAN helps... 

Uplink: try to get it sponsored from some local small ISPs, but also have some backup line from a big ISP that is less likely to fail, also make sure that the small ISP has differenct uplinks, esp if you take different ones. Universities can also give big bandwidth. 



### nice-to-have: POC / GSM

If you want to appeal to DECT: eventphone.de

DECT equipment is pretty expensive, GSM needs licenses. 


### Food/Drinks

* Have free drinking water (if the tap-water doesnt serve)
* Clarify concessions and tax-stuff of you country if you wanna sell alcohol / soft drinks (selling makes shitload of work, but also a good additional "income")
* Depending if "mate" is reachable on the region around the venue you might wanna sell it at the event itself
* Similar for food. Some venues have "must have caterers" which are usually expensive and not good food
* all-day-breakfast-stuff is also a solution to get your ppl fed. maybe a have a deal with a pizza-delivery-services, eg all pizza for same prices, and they also deliver late night. 
* food stuff depends on many many factors: 
    * is your venue located where there is lots of food/restaurants around, or not?
    * has the venue a in house caterer you have to book?
    * is it a camp, where ppl might be able preparing themselves?
    * is your "outer culture" a place where ppl would expect food at such events?
    * (lots more)
* think about reasonable pricing: high enough to earn / dont loss, but not too high otherwise ppl will bring their own stuff
* be careful with deposits, its hackers, so ppl might be bringing stuff from outsite if its too high. 

* for cocktails and stuff you need much more logistics
* generally lots of logistics with drinks
* dont forget you have to deal with much more cash-money then as without. think about tons of coins you need to get to give out suitable change. 
* think about any token system. Its easier for the organizers, but stupid for the participants



### nice-to-have: Audio, Video, Speech Technology, Streaming

On the events in the audio hardware from a Location Provider or a local service provider. For video recording and streaming, a working group between the individual detection has been established in [1] to reach. Contact person Andi and derPeter. There is also a currently yet to be filled, public Wiki https://voc.media.ccc.de/wiki/ .

Cameras are usually rented through a local service provider, or in the case of universities borrowed. For the recording of the slides (VGA or DVI) of the CCCV frame grabber procured, which are listed under inventory.

* recordings need lots of ressources: Cameras, wires, adapters, and much of people/volunteers, esp. with post-productions. 

* trick for ad-hoc-PA-system: mobilphone and bluethooth speakers. 

## nice-to-have: Decoration

* caring for lights already makes a nice atmosphere
* Deco can be very expensive
* Ideally find ressources you can get for free like from university-party-groups
* even better finding party-collectives that would like to decorate your event
* lots of projectors also help, also "dia projectoren"
* Think about stuff you get that it might break / get lost

### nice-to-have: Outreach and CD

* In this task you have to handle lots deadlines: shirts need to be done in time, wristbands need to be there early, but if you wanna have a handout on site with the schedule you wanna print it as late as possible. 
* Ask any Designer ideally from the "family", maybe somewhat peripherical. You can also make a Call For Design to start
* You can decide on motto for the event to get better ideas on a design
* license of grafic stuff should be open, so that ppl remix
* Stuff that can / needs to be done: 
  - flyers
  - wristbands
  - posters
  - signs
  - shirts
  - website/ weblog / wiki
  - web-banners
  - sticker
  - lanyards
  - font for the CD, ideally an open font
  - Wallpapers
  - schedule design
  - Trailer/Animations
  - signs (directions)
  - speaker's desk

Order printmaterial has early deadlines: mugs, T-Shirts, website, stickers, Badges, Wristbands


### Content Team / CfP

* language: 
* Tracks
* how many rooms / how big
* 1 room for "talks" and 1-2 more, HC+lounge


### CERT / Security

xxxxxx It is always good - even in smaller events for CERT and Security contact person before event, and to have in place.

For CERT you can log in https://cert.ccc.de .


### Legal issues
you might wanna have a person/team that cares for all the legal stuff
* drinks, food (preparing), founding a legal entity?, CERT/security issues, abuse-cases (not only) in the network


### The cashier / intake controls / COC

The CCCV has

A Set of five registers in flight cases
A Server (can also be used by Laptop replaced)

This Hardware is for Events with a lot of cash transactions (entry, Goodie-sale). In many prior messages, or lack of participants with a computer, and if necessary, a hand-held scanner or a bar list better. The POS system is integrated with the pretix. If eh pretix is used in the pre-sale, but little cash is handled, can also be the inlet-App pretix.

Point of contact
(best) Treasurer (CCC e. V.)
Location
Berlin

### Presale / Ticket Sales

The CCCV is for the time (2017) pretix.

Point of contact
Treasurer (CCCV)


### Location/Venue

 Should be done in the ideal case _weit_ before
Access 24/7
Either the city centre or with good public TRANSPORT connection and Parking facilities
Hackers can also survive in the cage. Bunkers are popular.
Hack center
A lecture hall per Track
Workshop rooms
Lounge/Bar, brings good money
Sleeping places are a plus point
Plus Point: Chill Out Outdoor Area

Owner-things: it'll be all fine once the event is there. 


### (Pre-) Financing

* for small scale events where people trust each other, you should check out the Camp++ community payment protocol
    * https://www.ctrlc.hu/~stef/rfc-cpp.txt
* otherwise just start a proper budgeting and make an estimate what the price for ticket should be
* decide if you need the money beforehand and have a "pre-sale" or if its okay to get the money from ticket sale at the door only
* what if you're afraid that the event might fail financially?
    * one trick is to found any legal entity that goes bankrupt then, without affecting the legal entity that the hackerspace/group has so far

* sell drinks


### Payment models

In this Chapter, the advantages and disadvantages of the current payment models will be explained.


#### Official Entry

The official admission is charged at the entrance or via a pre-sale (or and). It is highly recommended to have a low entry level (think: study/student/ALG-compatible), and a cost ceiling to take admission as a higher level, if necessary, with additional incentive through the Goodies. To write to a level that it covers the cost of the event, can help here.

Advantages:

To calculate
In the case of advance cash little equity is needed.

Used, for example. in the CCCV events, at Easterhegg.


#### Voluntary compulsory charity model

It is required for the event, no admission. However, you will be asked to pay for the fixed costs of the event, a "voluntary" donation. A minimum suggested amount to be provided as a thank you Gimmicks the donor. Are suitable for Badges, cheap merchandise (Cup), coffee Flat.

Other merchandising items can, regardless of the donation is to be sold.

Advantages:

There are no participants will be excluded who cannot afford admission
No access checks needed

Suitable for:

 Small to medium-sized events
For events with more of a focus on Club-Internal (because of higher willingness to donate)

Used, for example. from the Entropia e. V. for the GPN.


#### Self-assessment

For all applicable Items, a donation is placed in the box and the participants will be informed about all the costs that are incurred. All participants are free to decide how much can be contributed to the relevant Items. It is perfectly in order, that there are participants who can not or only little contribute, and therefore of the affluent Nerds co-Finance. This model is recommended for small events up to 50 participants in a more familiar environment.

Advantages:

More money coming in than in the case of fixed prices
 As well as no effort

Cons:

Risk, everything is based on trust

Suitable for:

small events with a low Cost
Family events (because of the higher willingness to donate)
Geekends and smaller parties



# Step-by-Step guide in planning
- venue
- date
- cfp
- bringing power, net, talks



This Text is based on the experience from Hamburg, so it should be used as a rough guideline. - your mileage may vary.

## (T-3 Months) Early/Before

(starting depends on many factors, eg doing it first time / regularly):

Call for Papers out
Frab set up
Premises for rent
Design 


## T-3 Months

Power supply check and, if necessary, with the NOC to the local $Telecom operator appeal. (Contact person for HH:falk and User:Vt100)

Space for POC/NOC schedule, do not have to sit in the aisle.

Holiday event (eg, EH): bakeries contact, to identify who is working over the holidays, and rolls. (in HH bakery hail stone, the have a branch right next to the house of a citizen)

Drinks: order and delivery - represent the holidays. (in Hamburg: either the supplier of the attractor or of the beer-specific high air)


## T-2 Months

The website should be slow - now, at least, registration should be open. Data economy / sparsamkeit! 
ask for T-Shirt-size is clever, because you can then order.

Verpeiler into consideration!!!

Timetable: pre-release version may be out now, if possible (Yes, lectures come in always at the last Minute.) Two or three presentations from the Workshop in the behind hand, if a speaker cancels, gets sick or stuck in traffic.


## T-1 Month

If you haven't already, you should now order the materials. Who does not have a printer at Hand: Fred pressure in Darmstadt, Germany, is the die-hard Chaos printer: [[2]]

In addition, it is recommended to by now, again in the premises of audition, if everything works out, and to do it again the ne ascent.


## T-1 Days

Per room there is a sufficiently strong image projectors, for safety's sake, one in Reserve (murphy). Adapter for macbooks hold.

Computer for the cash register with a simple script/interface, Name, Member or non-Member to check if there.

Printer with sufficient Toner and paper to print maps and signs.

Drinks cold.

Shopping/purchases store.

Exchange to get more money.


## T-0 Production

You want a OrgaOffice / the back room, and a money zählbrett / coin counter (small variant in the Office user:Dodger

enough money cassettes (number of stations +1 swap)

Sufficient angel (10 per 100 users)

Bun must in the morning be picked up, and Breakfast to be built.

The Organization should 24 be someone on-site, night-shifts arranged.

At least to the beginning of the Talks, an angel in the lecture rooms due to the audio/image projectors setup / technology foo

Grilling has proved to be a nice Social Event, because you can use up the leftover bread from Breakfast :)



# Dump of list of teams at C3 Congress: 

Assemblies	Assemblieplanung. Menschen, Themen, Sensationen

Auf- und Abbau

Awarenessteam	Vermittelt und unterstützt, wenn sich jemand belästigt oder diskriminiert fühlt

BOC	Bar, Beer, Booze, Beverage Operation Center

c3bottles	Koordination Pfand, Koordination Sammelengel, Sortierung und Lagerung in Halle 4

c3nav	Kartografie, Navigation und Ortung

c3sign (Beschilderung)	Routing der Menschen durch das Chaos den Congress mit Schildern.

CERT	Sanitäter, Doktoren, Feuerpatschen und sowas

Chaos Mentors	Support for C3 Newcomers

shell beach CCL unten (-1)	

Garderobe CLOC	lagert Kram der Besucher

Content	Planung des Vortragsprogramm in den großen Sälen

Einkauf	Einkauf. Wunder in 3h, unmögliches in 24.

Food	Essensversorgung der Engel

Freies CongressRadio	Produktion von Radiobeiträgen fuer die Freien Radios

Gelb	Hygiene Beratung >>> Bündelung Bestellung Hygienebedarf (passendes Desi / Handschuhe etc)

Süßkram	Fertig verpackte Snacks (Süßkram,...)

GSM	Wir machen $dinge mit GSM maybe more

Haecksen	Haecksen are the mailing list based female community of the CCC.

Heralds und Stagekoordinatoren (SHOC)	Wir kümmern uns um alle Heralde und Stagekoordinatoren.

Himmel	Here be angels.

Hinterzimmer	Zimmert hinter.

Infodesk	Anlaufpunkt für Kongressbesucher mit Informationsdefizit

Junghackertag	Junghackertag am Tag 2 mit zahlreichen Workshops für Kinder und Jugendliche bis 14 Jahre

Kasse	Tauscht QR-Code gegen Bändchen

Kidspace	Space at the congress for kids and parents.

Kreatur	Koordination Bau - CCL Glashalle

Lightning Talks	Organisation und Moderation der Lightning Talks

LOC	Logistic and buildup/teardown coordination for the c3

Lounges	Kunst, Kultur, Musik, Party! \o_ _o/ \o/ \o_ _o/

NOC	Macht Internet.

NOC Helpdesk	The NOC helpdesk...

POC	DECT und Festnetztelefone, aka klingeling

Presale	Vorbereitung und Durchführung des Vorverkaufs

Pressebetreuung	Koordination Presseraum, Medien-Engel und Journalisten

Projektleitung	Rahmenbau, Konzeptionierung, Koordination und Überblick

Funk	Providing DMR digital radio services.

Schlüssel	Schlüsselverwaltung und Ausgabe der vom Betreiber ausgebenen Schlüssel

Security	(Physical) Security & Problem solving Team

Seidenstrasse	Seidenstrasse von A nach B

Spätkauf	verkauft spät bis früh

Speakers Desk	Speaker Anmeldung und Betreuung, Fahrplan Organisation/Änderungen während der Veranstaltung.

Strom	Stromversorgung der Teilnehmer

Subtitles	Macht Untertitel

Vereinstisch	Mitgliederverwaltungsanlaufstelle

Video	Organisation, Bereitstellung, Aufbau, Betrieb und Abbau der Video-Aufzeichnungs- und Streaming-Technik.

Wiki	Wir bauen das Public Wiki und administrieren es.


24hrs? 1p responsible at a given time. 



# Notes 

food

kalendar https://pads.ccc.de/events
https://md.gnom.space/radar

wie kriegt man das hin dass da nur die richtigen Veranstaltungen drin sind. 


ausländische chaostreffs / erfas 

etcheidungsdiffusionsstrukturen aufm congress

verantwortungsdiffusion 


